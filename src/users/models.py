from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import String, Column, Integer

Base = declarative_base()


class User(Base):
    __tablename__ = "users"

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    first_name = Column("first_name", String(length=50), nullable=False)
    last_name = Column("last_name", String(length=50), nullable=False)
    age = Column("age", Integer, nullable=False)

    def __init__(self, first_name: str, last_name: str, age: int):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def __repr__(self) -> str:
        return (
            "(id: {id}, "
            + "first_name: {first_name}, "
            + "last_name: {last_name}, "
            + "age: {age})"
        ).format(
            id=self.id,
            first_name=self.first_name,
            last_name=self.last_name,
            age=self.age,
        )
