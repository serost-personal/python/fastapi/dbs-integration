import os
import sys
from dotenv import load_dotenv

if not load_dotenv():
    print(".env file not found!")

db_host = os.environ.get("DB_HOST")
db_port = os.environ.get("DB_PORT", "5432")
db_user = os.environ.get("DB_USER")
db_pass = os.environ.get("DB_PASS")
db_name = os.environ.get("DB_NAME", "postgres")

redis_host = os.environ.get("REDIS_HOST")
redis_port = os.environ.get("REDIS_PORT", "6379")
