from fastapi import FastAPI
from contextlib import asynccontextmanager

from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from redis import asyncio as aioredis

from src.users.router import router as users_router

from src.config import redis_host, redis_port


@asynccontextmanager
async def lifespan(app: FastAPI):
    # Load
    redis = aioredis.from_url(f"redis://{redis_host}:{redis_port}")
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")
    yield
    # Clean up


app = FastAPI(title="DBs Integration", lifespan=lifespan)

app.include_router(users_router)
