# GENERATE NEXT REVISIONS WITH --autogenerate
# I WILL FUCKING KILL MYSELF TODAY
"""Initial revision

Revision ID: 4ecd42f030ee
Revises: 
Create Date: 2024-02-13 19:37:46.588168

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = "4ecd42f030ee"
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
